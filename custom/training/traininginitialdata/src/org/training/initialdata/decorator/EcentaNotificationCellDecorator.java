package org.training.initialdata.decorator;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class EcentaNotificationCellDecorator implements CSVCellDecorator {

    @Override
    public String decorate(int i, Map<Integer, String> map) {
        String currect_message = map.get(i);
        currect_message = currect_message + " edited";
        return currect_message;
    }
}
