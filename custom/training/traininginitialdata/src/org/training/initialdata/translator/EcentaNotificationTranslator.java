package org.training.initialdata.translator;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.training.core.jalo.GeneratedEcentaNotification;

public class EcentaNotificationTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(String s, Item item) throws JaloInvalidParameterException {
        clearStatus();
        String title = s;
        if (title.length() > 100) {
            title = title.substring(0, 100);
        }
        return title;
    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return o == null ? "" : o.toString();
    }
}
