package org.training.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationPrepareInterceptor implements PrepareInterceptor<EcentaNotificationModel>
{
    @Override
    public void onPrepare(EcentaNotificationModel ecentaNotificationModel, InterceptorContext interceptorContext) throws InterceptorException
    {
        if(!ecentaNotificationModel.getRead()){
            ecentaNotificationModel.setDeleted(false);
        }
    }
}
