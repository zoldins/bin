package org.training.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationInitDefaultsInterceptor implements InitDefaultsInterceptor<EcentaNotificationModel>
{

    private PersistentKeyGenerator ecentaNotificationIdGenerator;

    @Override
    public void onInitDefaults(EcentaNotificationModel ecentaNotificationModel, InterceptorContext interceptorContext) throws InterceptorException
    {
        ecentaNotificationModel.setId(ecentaNotificationIdGenerator.generate().toString());
    }

    public PersistentKeyGenerator getEcentaNotificationIdGenerator()
    {
        return ecentaNotificationIdGenerator;
    }

    public void setEcentaNotificationIdGenerator(PersistentKeyGenerator ecentaNotificationIdGenerator)
    {
        this.ecentaNotificationIdGenerator = ecentaNotificationIdGenerator;
    }

}
