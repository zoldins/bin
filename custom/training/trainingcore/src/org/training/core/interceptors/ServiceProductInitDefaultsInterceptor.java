package org.training.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import org.apache.log4j.Logger;
import org.training.core.model.ServiceProductModel;

import java.util.Date;

public class ServiceProductInitDefaultsInterceptor implements InitDefaultsInterceptor<ServiceProductModel>
{

    private PersistentKeyGenerator serviceProductIdGenerator;
    private final static Logger LOG = Logger.getLogger(ServiceProductInitDefaultsInterceptor.class.getName());
    
    @Override
    public void onInitDefaults(ServiceProductModel serviceProductModel, InterceptorContext interceptorContext) throws InterceptorException
    {
        serviceProductModel.setId(serviceProductIdGenerator.generate().toString());
        serviceProductModel.setCreationDate(new Date());
        LOG.info("New id is :" + serviceProductModel.getId());
        LOG.info("Date is : " + serviceProductModel.getCreationDate().toString());
    }

    public PersistentKeyGenerator getServiceProductIdGenerator()
    {
        return serviceProductIdGenerator;
    }

    public void setServiceProductIdGenerator(PersistentKeyGenerator serviceProductIdGenerator)
    {
        this.serviceProductIdGenerator = serviceProductIdGenerator;
    }
}
