package org.training.core.attributes;

import de.hybris.platform.b2b.jalo.B2BUnit;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationB2BUnitAttributeHandler extends AbstractDynamicAttributeHandler<B2BUnit,EcentaNotificationModel>
{

    private ModelService modelService;

    @Override
    public B2BUnit get(final EcentaNotificationModel model){
        return getModelService().getSource(model.getB2bCustomer().getDefaultB2BUnit());
    }

    public ModelService getModelService()
    {
        return modelService;
    }

    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

}
