package org.training.core.crud;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import org.apache.log4j.Logger;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.dao.impl.CustomEcentaNotificationsDAOImpl;
import org.training.core.enums.EcentaNotificationPriorityEnum;
import org.training.core.enums.EcentaNotificationTypeEnum;
import org.training.core.model.EcentaNotificationModel;

import java.util.Date;

public class EcentaNotificationServiceImpl implements EcentaNotificationService
{
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;
    private ModelService modelService;

    private final static Logger LOG = Logger.getLogger(EcentaNotificationServiceImpl.class.getName());

    @Override
    public EcentaNotificationModel getEcentaNotificationModel(String id)
    {
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationById(id);
        checkIfEcentaNotificationExists(ecentaNotificationModel);
        return ecentaNotificationModel;
    }

    @Override
    public EcentaNotificationModel createEcentaNotificationModel(String id, B2BCustomer b2BCustomer, Date date, EcentaNotificationTypeEnum type, String message, EcentaNotificationPriorityEnum priority, String title)
    {
        EcentaNotificationModel ecentaNotificationModel = modelService.create("EcentaNotification");
        if (id != null) {
            ecentaNotificationModel.setId(id);
        }
        if (b2BCustomer != null) {
            ecentaNotificationModel.setB2bCustomer(modelService.get(b2BCustomer));
        }
        if (date != null) {
            ecentaNotificationModel.setDate(date);
        }
        if (type != null) {
            ecentaNotificationModel.setType(type);
        }
        if (message != null) {
            ecentaNotificationModel.setMessage(message);
        }
        if (priority != null) {
            ecentaNotificationModel.setPriority(priority);
        }
        if (title != null) {
            ecentaNotificationModel.setTitle(title);
        }
        boolean createdEcentaNotification = updateDatabase(ecentaNotificationModel, "save");
        return createdEcentaNotification ? ecentaNotificationModel : null;
    }

    @Override
    public boolean updateEcentaNotificationModel(String id, EcentaNotificationTypeEnum newType, String newMessage, EcentaNotificationPriorityEnum newPriority, String newTitle, Boolean newRead, Boolean newDeleted)
    {
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationById(id);
        checkIfEcentaNotificationExists(ecentaNotificationModel);
        if (newType != null) {
            ecentaNotificationModel.setType(newType);
        }
        if (newMessage != null) {
            ecentaNotificationModel.setMessage(newMessage);
        }
        if (newPriority != null) {
            ecentaNotificationModel.setPriority(newPriority);
        }
        if (newTitle != null) {
            ecentaNotificationModel.setTitle(newTitle);
        }
        if (newRead != null) {
            ecentaNotificationModel.setRead(newRead);
        }
        if (newDeleted != null) {
            ecentaNotificationModel.setRead(newDeleted);
        }
        return updateDatabase(ecentaNotificationModel, "save");
    }

    @Override
    public boolean updateEcentaNotificationReadValue(String id, boolean read)
    {
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationById(id);
        checkIfEcentaNotificationExists(ecentaNotificationModel);
        ecentaNotificationModel.setRead(read);
        return updateDatabase(ecentaNotificationModel, "save");
    }

    @Override
    public boolean updateEcentaNotificationDeletedValue(String id, boolean deleted)
    {
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationById(id);
        checkIfEcentaNotificationExists(ecentaNotificationModel);
        ecentaNotificationModel.setDeleted(deleted);
        return updateDatabase(ecentaNotificationModel, "save");
    }

    @Override
    public boolean deleteEcentaNotificationModel(String id)
    {
        EcentaNotificationModel ecentaNotificationModel = customEcentaNotificationsDAO.findEcentaNotificationById(id);
        checkIfEcentaNotificationExists(ecentaNotificationModel);
        return updateDatabase(ecentaNotificationModel, "remove");
    }

    private void checkIfEcentaNotificationExists(EcentaNotificationModel ecentaNotificationModel)
    {
        if (ecentaNotificationModel == null) {
            try {
                throw new UnknownEcentaNotificationException();
            } catch (UnknownEcentaNotificationException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean updateDatabase(EcentaNotificationModel ecentaNotificationModel, String operation)
    {
        Transaction tx = null;
        try {
            tx = Transaction.current();
            tx.begin();
            if (operation.equals("remove")) {
                getModelService().remove(ecentaNotificationModel);
            } else if (operation.equals("save")) {
                getModelService().save(ecentaNotificationModel);
            }
            tx.commit();
            return true;
        } catch (final ModelSavingException e) {
            if (null != tx) {
                tx.rollback();
            }
            return false;
        }
    }

    public CustomEcentaNotificationsDAO getCustomEcentaNotificationsDao()
    {
        return customEcentaNotificationsDAO;
    }

    public void setCustomEcentaNotificationsDao(final CustomEcentaNotificationsDAO customProductsDao)
    {
        this.customEcentaNotificationsDAO = customProductsDao;
    }

    public ModelService getModelService()
    {
        return modelService;
    }

    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    public static class UnknownEcentaNotificationException extends Exception
    {
    }

}
