package org.training.core.crud;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import org.training.core.enums.EcentaNotificationPriorityEnum;
import org.training.core.enums.EcentaNotificationTypeEnum;
import org.training.core.model.EcentaNotificationModel;

import java.util.Date;

public interface EcentaNotificationService
{
    /**
     * This function returns the EcentaNotificationModel with a specified id.
     * @param id the id of the EcentaNotification
     * @return an EcentaNotificationModel with the corresponding id
     */
    EcentaNotificationModel getEcentaNotificationModel(String id);

    /**
     * This function creates an EcentaNotification and returns an EcentaNotificationModel that was created.
     * All attributes except id can be null.
     * @param id The id of the EcentaNotification.
     * @param b2BCustomer The associated b2bCustomer of the EcentaNotification. (can be null)
     * @param date The associated date of the EcentaNotification. (can be null)
     * @param type The associated EcentaNotificationType of the EcentaNotification. (can be null)
     * @param message The associated message of the EcentaNotification. (can be null)
     * @param priority The associated EcentaNotificationPriority of the EcentaNotification. (can be null)
     * @param title The associated title of the EcentaNotification. (can be null)
     * @return The EcentaNotificationModel that was created. (can be null)
     */
    EcentaNotificationModel createEcentaNotificationModel(String id, B2BCustomer b2BCustomer, Date date,
                                                          EcentaNotificationTypeEnum type, String message,
                                                          EcentaNotificationPriorityEnum priority, String title);

    /**
     * This functions updates a specified EcentaNotification from the database.
     * @param id The id of the EcentaNotification.
     * @param type the new type of the EcentaNotification (can be null for no update to this field.)
     * @param message the new message of the EcentaNotification (can be null for no update to this field.)
     * @param priority the new priority of the EcentaNotification (can be null for no update to this field.)
     * @param title the new title of the EcentaNotification (can be null for no update to this field.)
     * @param read the new read status of the EcentaNotification (can be null for no update to this field.)
     * @param deleted the new deleted status of the EcentaNotification (can be null for no update to this field.)
     * @return true if update successful, false - if not.
     */
    boolean updateEcentaNotificationModel(String id, EcentaNotificationTypeEnum type, String message,
                                          EcentaNotificationPriorityEnum priority, String title, Boolean read, Boolean deleted);

    /**
     * This functions updates the read value of a specified EcentaNotification from the database.
     * @param id The id of the EcentaNotification.
     * @param read the new read status of the EcentaNotification
     * @return true if update successful, false - if not.
     */
    boolean updateEcentaNotificationReadValue(String id, boolean read);

    /**
     * This functions updates the deleted value of a specified EcentaNotification from the database.
     * @param id The id of the EcentaNotification.
     * @param deleted the new deleted status of the EcentaNotification
     * @return true if update successful, false - if not.
     */
    boolean updateEcentaNotificationDeletedValue(String id, boolean deleted);

    /**
     * This functions deletes a specified EcentaNotification from the database.
     * @param id The id of the EcentaNotification to be deleted.
     * @return true if delete successful, false - if not.
     */
    boolean deleteEcentaNotificationModel(String id);
}
