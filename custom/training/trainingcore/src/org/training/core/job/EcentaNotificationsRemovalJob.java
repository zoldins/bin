package org.training.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.EcentaNotificationRemovalCronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.tx.Transaction;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.model.EcentaNotificationModel;

public class EcentaNotificationsRemovalJob extends AbstractJobPerformable<EcentaNotificationRemovalCronJobModel>
{
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;
    private ModelService modelService;

    private final static Logger LOG = Logger.getLogger(EcentaNotificationsRemovalJob.class.getName());

    @Override
    public PerformResult perform(EcentaNotificationRemovalCronJobModel ecentaNotificationRemovalCronJobModel)
    {
        final int noOfDaysOldToRemove = ecentaNotificationRemovalCronJobModel.getXDaysOld();
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -noOfDaysOldToRemove);
        final Date oldDate = cal.getTime();
        final List<EcentaNotificationModel> ecentaNotificationModelList = customEcentaNotificationsDAO.findAllEcentaNotificationsOlderThanSpecifiedDays(oldDate);
        LOG.debug("Ecenta Notifications older than specified days size:" + ecentaNotificationModelList.size());
        for (final EcentaNotificationModel ecentaNotificationModel : ecentaNotificationModelList)
        {
            ecentaNotificationModel.setRead(true);
            ecentaNotificationModel.setDeleted(true);
        }

        if (!CollectionUtils.isEmpty(ecentaNotificationModelList))
        {
            Transaction tx = null;
            try
            {
                tx = Transaction.current();
                tx.begin();
                getModelService().saveAll(ecentaNotificationModelList);
                tx.commit();
            }
            catch (final ModelSavingException e)
            {
                if (null != tx)
                {
                    tx.rollback();
                }
                LOG.error("Could not update the ecenta notifications list -->" + e);
            }
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public CustomEcentaNotificationsDAO getCustomEcentaNotificationsDao()
    {
        return customEcentaNotificationsDAO;
    }

    public void setCustomEcentaNotificationsDao(final CustomEcentaNotificationsDAO customProductsDao)
    {
        this.customEcentaNotificationsDAO = customProductsDao;
    }

    public ModelService getModelService()
    {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

}
