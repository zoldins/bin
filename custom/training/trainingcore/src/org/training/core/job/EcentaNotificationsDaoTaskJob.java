package org.training.core.job;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.EcentaNotificationDaoTaskCronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.tx.Transaction;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.enums.EcentaNotificationPriorityEnum;
import org.training.core.enums.EcentaNotificationTypeEnum;
import org.training.core.model.EcentaNotificationModel;

import java.util.List;

public class EcentaNotificationsDaoTaskJob extends AbstractJobPerformable<EcentaNotificationDaoTaskCronJobModel>
{

    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;
    private ModelService modelService;
    private FlexibleSearchService flexibleSearchService;

    private final static Logger LOG = Logger.getLogger(EcentaNotificationsDaoTaskJob.class.getName());

    @Override
    public PerformResult perform(EcentaNotificationDaoTaskCronJobModel ecentaNotificationDaoTaskCronJobModel)
    {
        final B2BCustomer b2BCustomer = getB2BCustomer("william.hunter@pronto-hw.com");
        final List<EcentaNotificationModel> ecentaNotificationModelListOnlyCustomer =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomer(b2BCustomer);
        final List<EcentaNotificationModel> ecentaNotificationModelListCustomerAndType =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndOrderManagementType(b2BCustomer);
        final List<EcentaNotificationModel> ecentaNotificationModelListCustomerAndPriority =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndHighPriority(b2BCustomer);

        LOG.debug("Ecenta Notifications with B2BCustomer (william.hunter@pronto-hw.com):" + ecentaNotificationModelListOnlyCustomer.size());
        LOG.debug("Ecenta Notifications with B2BCustomer (william.hunter@pronto-hw.com) and Type (ORDER MANAGEMENT):" + ecentaNotificationModelListOnlyCustomer.size());
        LOG.debug("Ecenta Notifications with B2BCustomer (william.hunter@pronto-hw.com) and Priority (HIGH):" + ecentaNotificationModelListOnlyCustomer.size());
        //For this Ecenta Notification with this B2BCustomer set title to "Imants Title"
        for (final EcentaNotificationModel ecentaNotificationModel : ecentaNotificationModelListOnlyCustomer) {
            ecentaNotificationModel.setTitle("Imants Title");
        }
        //For this Ecenta Notification with this B2BCustomer and type append ORDER MANAGEMENT to title
        for (final EcentaNotificationModel ecentaNotificationModel : ecentaNotificationModelListCustomerAndType) {
            ecentaNotificationModel.setTitle(ecentaNotificationModel.getTitle() + " ORDER MANAGEMENT");
        }
        //For this Ecenta Notification with this B2BCustomer and priority append HIGH to title
        for (final EcentaNotificationModel ecentaNotificationModel : ecentaNotificationModelListCustomerAndPriority) {
            ecentaNotificationModel.setTitle(ecentaNotificationModel.getTitle() + " HIGH");
        }

        saveModelsToDatabase(ecentaNotificationModelListOnlyCustomer);
        saveModelsToDatabase(ecentaNotificationModelListCustomerAndType);
        saveModelsToDatabase(ecentaNotificationModelListCustomerAndPriority);

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private B2BCustomer getB2BCustomer(String email){
        String queryStr = "SELECT {PK} FROM {B2BCustomer} WHERE {email} = ?email";
        FlexibleSearchQuery fsq = new FlexibleSearchQuery( queryStr );
        fsq.addQueryParameter("email", email);
        SearchResult<B2BCustomerModel> result = getFlexibleSearchService().search( fsq );
        List<B2BCustomerModel> B2BCustomerList = result.getResult();
        return getModelService().getSource(B2BCustomerList.get(0));
    }

    private void saveModelsToDatabase(List<EcentaNotificationModel> modelList)
    {
        if (!CollectionUtils.isEmpty(modelList)) {
            Transaction tx = null;
            try {
                tx = Transaction.current();
                tx.begin();
                getModelService().saveAll(modelList);
                tx.commit();
            } catch (final ModelSavingException e) {
                if (null != tx) {
                    tx.rollback();
                }
                LOG.error("Could not update the ecenta notifications list -->" + e);
            }
        }
    }

    public CustomEcentaNotificationsDAO getCustomEcentaNotificationsDao()
    {
        return customEcentaNotificationsDAO;
    }

    public void setCustomEcentaNotificationsDao(final CustomEcentaNotificationsDAO customProductsDao)
    {
        this.customEcentaNotificationsDAO = customProductsDao;
    }

    public ModelService getModelService()
    {
        return modelService;
    }

    @Override
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    public FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

    @Override
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }
}
