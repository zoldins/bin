package org.training.core.dao.impl;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.enums.EcentaNotificationPriorityEnum;
import org.training.core.enums.EcentaNotificationTypeEnum;
import org.training.core.model.EcentaNotificationModel;

import javax.annotation.Resource;

public class CustomEcentaNotificationsDAOImpl implements CustomEcentaNotificationsDAO
{
    private FlexibleSearchService flexibleSearchService;
    private ModelService modelService;
    private ConfigurationService configurationService;

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsOlderThanSpecifiedDays(final Date oldDate)
    {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {date}<=?oldDate");
        final Map<String, Object> params = new HashMap<>();
        params.put("oldDate", oldDate);

        return searchForEcentaNotifications(query, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomer(final B2BCustomer b2BCustomer)
    {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {b2bCustomer} = ?b2bCustomer AND {deleted} = 'false' ORDER BY {date} DESC");
        final Map<String, Object> params = new HashMap<>();
        params.put("b2bCustomer", getModelService().get(b2BCustomer));

        return searchForEcentaNotifications(query, params);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndHighPriority(B2BCustomer b2BCustomer)
    {
        return findAllEcentaNotificationsForSpecifiedB2BCustomerAndPriority(b2BCustomer, EcentaNotificationPriorityEnum.HIGH);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndOrderManagementType(B2BCustomer b2BCustomer)
    {
        return findAllEcentaNotificationsForSpecifiedB2BCustomerAndType(b2BCustomer, EcentaNotificationTypeEnum.ORDER_MANAGEMENT);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndNewsType(B2BCustomer b2BCustomer)
    {
        return findAllEcentaNotificationsForSpecifiedB2BCustomerAndType(b2BCustomer, EcentaNotificationTypeEnum.NEWS);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndServiceTicketsType(B2BCustomer b2BCustomer)
    {
        return findAllEcentaNotificationsForSpecifiedB2BCustomerAndType(b2BCustomer, EcentaNotificationTypeEnum.SERVICE_TICKETS);
    }

    @Override
    public List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndWorkflowType(B2BCustomer b2BCustomer)
    {
        return findAllEcentaNotificationsForSpecifiedB2BCustomerAndType(b2BCustomer, EcentaNotificationTypeEnum.WORKFLOW);
    }

    private List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndType(final B2BCustomer b2BCustomer, final EcentaNotificationTypeEnum typeEnum)
    {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {b2bCustomer} = ?b2bCustomer AND {type} = ?type AND {deleted} = 'false' ORDER BY {date} DESC");
        final Map<String, Object> params = new HashMap<>();
        params.put("b2bCustomer", getModelService().get(b2BCustomer));
        params.put("type", typeEnum);

        return searchForEcentaNotifications(query, params);
    }

    private List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndPriority(final B2BCustomer b2BCustomer, final EcentaNotificationPriorityEnum priorityEnum)
    {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {b2bCustomer} = ?b2bCustomer AND {priority} = ?priority AND {deleted} = 'false' ORDER BY {date} DESC");
        final Map<String, Object> params = new HashMap<>();
        params.put("b2bCustomer", getModelService().get(b2BCustomer));
        params.put("priority", priorityEnum);

        return searchForEcentaNotifications(query, params);
    }

    @Override
    public EcentaNotificationModel findEcentaNotificationById(String id)
    {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {id} = ?id");
        final Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        return searchForEcentaNotifications(query, params).get(0);
    }

    private List<EcentaNotificationModel> searchForEcentaNotifications(StringBuilder query, Map<String, Object> params)
    {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        searchQuery.setCount(configurationService.getConfiguration().getInt("ecentanotifications.showcount"));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    public FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }

    public ModelService getModelService()
    {
        return modelService;
    }

    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

}
