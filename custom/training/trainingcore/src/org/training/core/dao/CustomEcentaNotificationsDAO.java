package org.training.core.dao;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import org.apache.log4j.Logger;
import org.training.core.model.EcentaNotificationModel;

import java.util.Date;
import java.util.List;

public interface CustomEcentaNotificationsDAO
{
    /*
    Returns a list of all EcentaNotifications that are older than the specified date.
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsOlderThanSpecifiedDays(final Date oldDate);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomer(final B2BCustomer b2BCustomer);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer and HIGH priority
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndHighPriority(final B2BCustomer b2BCustomer);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer and Order Management type
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndOrderManagementType(final B2BCustomer b2BCustomer);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer and News type
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndNewsType(final B2BCustomer b2BCustomer);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer and Service Tickets type
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndServiceTicketsType(final B2BCustomer b2BCustomer);

    /*
    Returns a list of all EcentaNotifications that are linked to the specified B2BCustomer and Workflow type
     */
    List<EcentaNotificationModel> findAllEcentaNotificationsForSpecifiedB2BCustomerAndWorkflowType(final B2BCustomer b2BCustomer);

    /*
    Returns a EcentaNotification with a given id
     */
    EcentaNotificationModel findEcentaNotificationById(final String id);

}
