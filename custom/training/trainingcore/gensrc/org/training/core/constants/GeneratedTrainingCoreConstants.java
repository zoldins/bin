/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 19 Apr 2022, 13:40:59                       ---
 * ----------------------------------------------------------------
 */
package org.training.core.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingCoreConstants
{
	public static final String EXTENSIONNAME = "trainingcore";
	public static class TC
	{
		public static final String APPARELPRODUCT = "ApparelProduct".intern();
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String ECENTANOTIFICATION = "EcentaNotification".intern();
		public static final String ECENTANOTIFICATIONCOMPONENT = "EcentaNotificationComponent".intern();
		public static final String ECENTANOTIFICATIONDAOTASKCRONJOB = "EcentaNotificationDaoTaskCronJob".intern();
		public static final String ECENTANOTIFICATIONPRIORITYENUM = "EcentaNotificationPriorityEnum".intern();
		public static final String ECENTANOTIFICATIONREMOVALCRONJOB = "EcentaNotificationRemovalCronJob".intern();
		public static final String ECENTANOTIFICATIONTYPEENUM = "EcentaNotificationTypeEnum".intern();
		public static final String ELECTRONICSCOLORVARIANTPRODUCT = "ElectronicsColorVariantProduct".intern();
		public static final String SERVICEPRODUCT = "ServiceProduct".intern();
		public static final String SWATCHCOLORENUM = "SwatchColorEnum".intern();
	}
	public static class Attributes
	{
		public static class Order
		{
			public static final String ECENTANOTIFICATION = "ecentaNotification".intern();
		}
	}
	public static class Enumerations
	{
		public static class EcentaNotificationPriorityEnum
		{
			public static final String LOW = "LOW".intern();
			public static final String NORMAL = "NORMAL".intern();
			public static final String HIGH = "HIGH".intern();
		}
		public static class EcentaNotificationTypeEnum
		{
			public static final String ORDER_MANAGEMENT = "ORDER_MANAGEMENT".intern();
			public static final String NEWS = "NEWS".intern();
			public static final String SERVICE_TICKETS = "SERVICE_TICKETS".intern();
			public static final String WORKFLOW = "WORKFLOW".intern();
		}
		public static class SwatchColorEnum
		{
			public static final String BLACK = "BLACK".intern();
			public static final String BLUE = "BLUE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String GREY = "GREY".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String PINK = "PINK".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String RED = "RED".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String YELLOW = "YELLOW".intern();
		}
	}
	public static class Relations
	{
		public static final String ECENTANOTIFICATION2ORDER = "EcentaNotification2Order".intern();
	}
	
	protected GeneratedTrainingCoreConstants()
	{
		// private constructor
	}
	
	
}
