/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 19 Apr 2022, 13:40:59                       ---
 * ----------------------------------------------------------------
 */
package org.training.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "trainingfacades";
	
	protected GeneratedTrainingFacadesConstants()
	{
		// private constructor
	}
	
	
}
