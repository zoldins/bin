package org.training.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.*;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.training.core.model.EcentaNotificationModel;
import org.training.facades.product.data.EcentaNotificationData;

public class EcentaNotificationPopulator implements Populator<EcentaNotificationModel, EcentaNotificationData>
{

    private Converter<B2BCustomerModel, CustomerData> b2BCustomerConverter;
    private Converter<B2BUnitModel, B2BUnitData> b2BUnitConverter;

    @Override
    public void populate(EcentaNotificationModel source, EcentaNotificationData target) throws ConversionException
    {
        target.setId(source.getId());
        target.setB2bCustomer(getB2BCustomerConverter().convert(source.getB2bCustomer()));
        target.setDate(source.getDate());
        target.setType(source.getType());
        target.setMessage(source.getMessage());
        target.setPriority(source.getPriority());
        target.setRead(source.getRead());
        target.setDeleted(source.getDeleted());
        target.setTitle(source.getTitle());
        target.setB2bUnit(getB2BUnitConverter().convert(source.getB2bUnit()));
    }

    protected Converter<B2BCustomerModel, CustomerData> getB2BCustomerConverter()
    {
        return b2BCustomerConverter;
    }

    public void setB2BCustomerConverter(final Converter<B2BCustomerModel, CustomerData> b2bCustomerConverter)
    {
        b2BCustomerConverter = b2bCustomerConverter;
    }

    protected Converter<B2BUnitModel, B2BUnitData> getB2BUnitConverter()
    {
        return b2BUnitConverter;
    }

    public void setB2BUnitConverter(final Converter<B2BUnitModel, B2BUnitData> b2BUnitConverter)
    {
        this.b2BUnitConverter = b2BUnitConverter;
    }

}
