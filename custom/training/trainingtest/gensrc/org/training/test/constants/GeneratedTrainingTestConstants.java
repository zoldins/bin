/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 19 Apr 2022, 13:40:59                       ---
 * ----------------------------------------------------------------
 */
package org.training.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingTestConstants
{
	public static final String EXTENSIONNAME = "trainingtest";
	
	protected GeneratedTrainingTestConstants()
	{
		// private constructor
	}
	
	
}
