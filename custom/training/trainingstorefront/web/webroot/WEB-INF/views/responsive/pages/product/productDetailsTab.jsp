<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="tabhead">
    <a href="">${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="tab-container">
                    <div class="description">
                        <c:if test="${not empty product.description}">
                            <c:set var="descriptionVarFullText" value="${ycommerce:sanitizeHTML(product.description)}"/>
                            <c:choose>
                                <c:when test="${fn:length(product.description) > 60}">
                                    <c:set var="ellipsis" value="..."/>
                                    <c:set var="descriptionVar"
                                           value="${fn:substring(product.description, 0, (50 - fn:length(ellipsis)))}${ellipsis}"/>
                                    <div class="default-text">
                                            ${descriptionVar}</div>
                                    <div class="new-css-class-which-will-say-display-none-without-important-property js-show-full-text">
                                            ${descriptionVarFullText}
                                    </div>
                                    <button class="btn btn-primary js-pdp-show-more" data-text="Show less">
                                        Show more
                                    </button>
                                </c:when>
                                <c:when test="${fn:length(product.description) <= 60}">
                                    ${descriptionVar}
                                </c:when>
                            </c:choose>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

