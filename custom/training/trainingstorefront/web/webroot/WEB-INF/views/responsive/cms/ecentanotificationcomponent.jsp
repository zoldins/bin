<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div>
    <h3 class="text-center">Ecenta Notifications</h3>
    <div class="row">
        <button class="tablink col-md-2 open-notification-section-button" id="AllButton">
            <spring:theme code="text.account.ecentanotifications.ALL" />
        </button>
        <button class="tablink col-md-2 open-notification-section-button btn-primary " id="HighPriorityButton">
            <spring:theme code="text.account.ecentanotifications.HIGH_PRIORITY" />
        </button>
        <button class="tablink col-md-2 open-notification-section-button" id="OrderManagementButton">
            <spring:theme code="text.account.ecentanotifications.ORDER_MANAGEMENT" />
        </button>
        <button class="tablink col-md-2 open-notification-section-button" id="NewsButton">
            <spring:theme code="text.account.ecentanotifications.NEWS" />
        </button>
        <button class="tablink col-md-2 open-notification-section-button" id="ServiceTicketsButton">
            <spring:theme code="text.account.ecentanotifications.SERVICE_TICKETS" />
        </button>
        <button class="tablink col-md-2 open-notification-section-button" id="WorkflowButton">
            <spring:theme code="text.account.ecentanotifications.WORKFLOW" />
        </button>
    </div>
    <c:forEach items="${ecentaNotificationListList}" var="ecentaNotificationList">
        <div class="ecenta-notification-section display-none">
            <table class="table table-striped table-bordered">
                <c:forEach items="${ecentaNotificationList}" var="ecentaNotification">
                    <tr class="ecenta-table-row">
                        <c:choose>
                            <c:when test="${ecentaNotification.priority=='HIGH'}">
                                <td class="text-center"><b>!!!</b></td>
                            </c:when>
                            <c:when test="${ecentaNotification.priority=='NORMAL'}">
                                <td class="text-center"><b>!!</b></td>
                            </c:when>
                            <c:otherwise>
                                <td class="text-center">!</td>
                            </c:otherwise>
                        </c:choose>
                        <td class="text-center">
                            <fmt:formatDate value="${ecentaNotification.date}" pattern="dd-MM-YYYY"/>
                        </td>
                        <c:choose>
                            <c:when test="${ecentaNotification.read=='true'}">
                                <td class="text-center">${ecentaNotification.message}</td>
                            </c:when>
                            <c:otherwise>
                                <td class="text-center"><b>${ecentaNotification.message}</b></td>
                            </c:otherwise>
                        </c:choose>
                            <%-- Read icon --%>
                        <td class="text-center">
                            <button class="read-ecenta-button" notificationid="${ecentaNotification.id}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-check2" viewBox="0 0 16 16">
                                    <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
                                </svg>
                            </button>
                        </td>
                            <%-- Deleted icon --%>
                        <td class="text-center">
                            <button class="delete-ecenta-button" notificationid="${ecentaNotification.id}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-trash3" viewBox="0 0 16 16">
                                    <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                </svg>
                            </button>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </c:forEach>
</div>
