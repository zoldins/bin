ACC.training = {

    _autoload: [
        ["bindAddMoreBtn", $('.js-pdp-show-more').length > 0]
    ],

    bindAddMoreBtn: function () {
        $(".js-pdp-show-more").on("click", function (){
            $(".new-css-class-which-will-say-display-none-without-important-property").toggle();
            $(".default-text").toggle();
            let oldText = $(this).text();
            let newText = $(this).data('text');
            $(this).text(newText).data('text',oldText);
        });
    }
}