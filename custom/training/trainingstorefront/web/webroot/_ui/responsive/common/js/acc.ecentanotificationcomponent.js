ACC.ecentanotificationcomponent = {
    _autoload: [
        "loadEcentaNotifications",
        "openNotificationSection",
        "readEcentaNotification",
        "deleteEcentaNotification"
    ],

    loadEcentaNotifications: function () {
        $(function () {
            let divIds = ["All", "HighPriority", "OrderManagement", "News", "ServiceTickets", "Workflow"];
            $(".ecenta-notification-section").each(function (index) {
                $(this).attr('id', divIds[index]);
            });
            $("#"+divIds[1]).show();
        });
    },

    openNotificationSection: function () {
        $(document).on("click", '.open-notification-section-button', function () {
            $(".ecenta-notification-section").hide();
            $(".tablink").removeClass("btn-primary");
            let sectionName = $(this).attr('id');
            sectionName = "#" + sectionName.substring(0, sectionName.length - 6);
            $(sectionName).show();
            $(this).addClass("btn-primary");
            let sectionTable = $(sectionName).children().eq(0);
            if (sectionTable.children().length === 0) {
                sectionTable.html("<p class=\"text-center\">There are no ecenta notification for this category</p>");
            }
        });
    },

    deleteEcentaNotification: function () {
        $(document).on("click", '.delete-ecenta-button', function () {
            let button = $(this);
            let url = ACC.config.encodedContextPath + "/view/EcentaNotificationComponentController/deleteEcentaNotification/" + $(this).attr('notificationid');
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function () {
                    alert("Delete was successful!");
                    button.closest('.ecenta-table-row').remove();
                },
                error: function () {
                    alert("Delete failed!");
                }
            });
        });
    },

    readEcentaNotification: function () {
        $(document).on("click", '.read-ecenta-button', function () {
            let button = $(this);
            let url = ACC.config.encodedContextPath + "/view/EcentaNotificationComponentController/readEcentaNotification/" + $(this).attr('notificationid');
            $.ajax({
                url: url,
                type: 'PATCH',
                success: function () {
                    alert("Read was successful!");
                    let messageTd = button.closest('.ecenta-table-row').children().eq(2);
                    if (messageTd.html().includes("<b>")) {
                        messageTd.html(messageTd.html().substring(3, messageTd.html().length - 4));
                    }
                },
                error: function () {
                    alert("Read failed!");
                }
            });
        });
    }
}