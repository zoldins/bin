package org.training.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@RequireHardLogIn
@Controller
@RequestMapping("/ecentanotifications")
public class EcentaNotificationsPageController extends AbstractPageController
{

    private static final String ECENTA_NOTIFICATIONS_CMS_PAGE = "ecentaNotificationsContentPage";

    @RequestMapping(method = RequestMethod.GET)
    public String getEcentaNotifications(final Model model) throws CMSItemNotFoundException
    {
        final ContentPageModel ecentaNotificationsCMSPage = getContentPageForLabelOrId(ECENTA_NOTIFICATIONS_CMS_PAGE);
        storeCmsPageInModel(model, ecentaNotificationsCMSPage);
        setUpMetaDataForContentPage(model, ecentaNotificationsCMSPage);
        return getViewForPage(model);
    }

}
