package org.training.storefront.controllers.cms;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.training.core.crud.EcentaNotificationService;
import org.training.core.dao.CustomEcentaNotificationsDAO;
import org.training.core.model.EcentaNotificationModel;
import org.training.core.model.components.EcentaNotificationComponentModel;
import org.training.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller("EcentaNotificationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.EcentaNotificationComponent)
public class EcentaNotificationComponentController extends AbstractAcceleratorCMSComponentController<EcentaNotificationComponentModel>
{

    @Resource(name = "modelService")
    private ModelService modelService;

    @Resource(name = "b2bCustomerService")
    private B2BCustomerService<B2BCustomerModel, B2BUnitModel> customerService;

    @Resource(name = "customEcentaNotificationsDao")
    private CustomEcentaNotificationsDAO customEcentaNotificationsDAO;

    @Resource(name = "ecentaNotificationService")
    private EcentaNotificationService ecentaNotificationService;

    @Override
    protected void fillModel(HttpServletRequest request, Model model, EcentaNotificationComponentModel component)
    {
        B2BCustomer b2BCustomer = modelService.getSource(customerService.getUserForUID(request.getRemoteUser()));

        List<EcentaNotificationModel> ecentaNotificationAllList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomer(b2BCustomer);

        List<EcentaNotificationModel> ecentaNotificationHighPriorityList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndHighPriority(b2BCustomer);

        List<EcentaNotificationModel> ecentaNotificationOrderManagementList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndOrderManagementType(b2BCustomer);

        List<EcentaNotificationModel> ecentaNotificationNewsList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndNewsType(b2BCustomer);

        List<EcentaNotificationModel> ecentaNotificationServiceTicketsList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndServiceTicketsType(b2BCustomer);

        List<EcentaNotificationModel> ecentaNotificationWorkflowList =
                customEcentaNotificationsDAO.findAllEcentaNotificationsForSpecifiedB2BCustomerAndWorkflowType(b2BCustomer);

        List<List<EcentaNotificationModel>> ecentaNotificationListList = new ArrayList<>();

        ecentaNotificationListList.add(ecentaNotificationAllList);
        ecentaNotificationListList.add(ecentaNotificationHighPriorityList);
        ecentaNotificationListList.add(ecentaNotificationOrderManagementList);
        ecentaNotificationListList.add(ecentaNotificationNewsList);
        ecentaNotificationListList.add(ecentaNotificationServiceTicketsList);
        ecentaNotificationListList.add(ecentaNotificationWorkflowList);

        model.addAttribute("ecentaNotificationListList", ecentaNotificationListList);
    }

    @PatchMapping(value = "/readEcentaNotification/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void readNot(@PathVariable final String id)
    {
        ecentaNotificationService.updateEcentaNotificationReadValue(id, true);
    }

    @DeleteMapping(value = "/deleteEcentaNotification/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void deleteNot(@PathVariable final String id)
    {
        ecentaNotificationService.updateEcentaNotificationDeletedValue(id, true);
    }

}
